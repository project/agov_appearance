aGov Appearance
===============

This module provides an additional permission 'Administer theme settings' to
allow finer grained control over which roles can change theme settings.

SETUP
=====

Enable the module and configure which roles have 'Administer theme settings'
permissions.
